#!/bin/sh

set -e

BASE="$PWD"
FASL="$PWD/fasl/init.fasl"
VM="$PWD/bin/vm-simple"
OL="$PWD/owl/ol.scm"

cd ext

clean() {
    printf "" > $1
}

clean functions.c
clean dispatch.c
clean feature.list
clean libraries.scm
clean CFLAGS
clean LDFLAGS


# find enabled extensions

if [ "$1" = 'simple' ]; then
    echo "Creating simple, host VM code"
    possible=unix
else
    echo "Creating VM code"
    possible=*
fi

for EXT in $possible
do
   test -f $EXT/enabled || continue
   echo " - enabled $EXT"
   if [ -f $EXT/build.scm ]; then
       # owl-only extension, use vm-simple to build it
       ( cd $EXT && $VM $FASL $OL -i $BASE -e ',load "build.scm"' > build.log )
   fi;
   [ -f $EXT/main.c ]     && cat $EXT/main.c >> functions.c
   [ -f $EXT/dispatch.c ] && cat $EXT/dispatch.c >> dispatch.c
   [ -f $EXT/CFLAGS ]     && tr '\n' ' ' < $EXT/CFLAGS >> CFLAGS
   [ -f $EXT/LDFLAGS ]    && tr '\n' ' ' < $EXT/LDFLAGS >> LDFLAGS

   echo $EXT >> feature.list
   for scm in $EXT/*.scm; do
       tr -d '\n' < $scm \
           | grep -Eo '\(define-library +\([^\)]+)' \
           | sed 's/define-library\(.*\)/import\1)/' >> libraries.scm
   done
done

cd ..

# inline the content to correct place

cat c/ovm.h c/ovm.c \
   | sed -e "/\/\/ EXTENSION FUNCTIONS HERE/{r ext/functions.c" -e "d" -e "}" \
   | sed -e "/\/\/ EXTENSION DISPATCH HERE/{r ext/dispatch.c" -e "d" -e "}" \
   > c/_vm.c

