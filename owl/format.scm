#| doc
Formatting

This library provides a string formatting feature. Its `format` function
is compatible with `format` in many Scheme implementations. The
specification is

```
    (format [destination] format-string [arguments ...])
```

writes the characters of format-string to destination, except that a
tilde (~) introduces a format directive. The character after the tilde,
possibly preceded by prefix parameters and modifiers, specifies what
kind of formatting is desired. Most directives use one or more arguments
to create their output; the typical directive puts the next argument
into the output, formatted in some special way. It is an error if no
argument remains for a directive requiring an argument, but it is not
an error if one or more arguments remain unprocessed by a directive.

The output is sent to destination. If destination is #f, a string is created
that contains the output; this string is returned as the value of the call
to format. If destination is a string, then it is interpreted as the
format-string, and #f is implied as the omitted destination. If destination is
a list or null, then the result is a list of runes with the output of format
prepended to destination in the same manner as Owl's other render functions.
In all other cases format returns an unspecified value.
If destination is #t, the output is sent to the current output port. Otherwise,
destination must be an output port, and the output is sent there.

This implementation is mostly compatible with these directives from
SRFI 48: Intermediate Format Strings...

| DIRECTIVE	    | MNEMONIC | CONSUMES? | ACTION |
| :---:         | :---     | :---:     | :---   |
|    ~[w]a      | Any	         | yes   | (display arg) for humans |
|    ~[w]s      | Slashified   | yes   | (write arg) for parsers |
|    ~[w[,'c]]d | Decimal	     | yes   | output numeric arg in decimal radix |
|    ~[w[,'c]]x | heXadecimal	 | yes   | output numeric arg in hexdecimal radix |
|    ~[w[,'c]]o | Octal	       | yes   | output numeric arg in octal radix |
|    ~[w[,'c]]b | Binary	     | yes   | output numeric arg in binary radix |
|    ~c         | Character	   | yes   | output the single character arg |
|    ~[w[,d]]f	| Fixed	       | yes   | output numeric arg in fixed point decimal format |
|    ~~         | Tilde	       | no    | output a tilde	|
|    ~t         | Tab	         | no    | output a tab character |
|    ~%         | Newline	     | no    | output a newline character |
|    ~&         | Freshline	   | no    | output a newline character |
|    ~_         | Space	       | no    | a single space character is output |

The alphabetic directives (a,s,d,x,o,b,c,f) are case insensitive.

Note that the `[w]` and `[w[,c]]` prefix parameters are extensions to SRFI 48,
but common to many other implementations. Note: to simplify the code, all of the
directives accept up to two prefix parameters, but those directive that aren't
defined to use them simply silently ignore them.

The w prefix parameter, which consists of decimal digits, specifies a width for
the output of its arg. The output is padded to the left with spaces by default.
For those directives that take a 'c prefix as well, c is a character that is used
in place of space to pad the output. Example: "0x~9,'0x "

The ~f directive outputs a number with width w and d digits after the decimal;
~wf outputs a string or number with width w as above. Space is always used as
the pad character. The last digit of the output value is rounded for rationals.

In SRFI 48 ~& is specified to output a newline if it is known that the previous
output was not a newline. (Since I don't know how to do that in Owl), this
implementation always outputs a newline.

Extensions:

| DIRECTIVE	    | MNEMONIC  | CONSUMES? | ACTION |
| :---:         | :---      | :---:     | :---   |
|    ~<newline> | Ignore ws | yes*      | ignores all following whitespace |

Example:

```
(format #f "This is a very long format string, so we'll break it here ~
                     so it is more readable ~a" 'ok?)
```

Not Implemented:

| DIRECTIVE	    | MNEMONIC | CONSUMES? | ACTION |
| :---:         | :---     | :---:     | :---   |
|    ~y         | Yuppify	    | yes  | the list arg is pretty-printed to the output |
|    ~w         | WriteCirc	  | yes  | like ~s, but handles recursive structures |
|    ~h         | Help        | no   | call synopsis |
|    ~?         | Indirection | 2    | format is called recursively |
|    ~K         | Indirection | 2    | ditto |

~K ~?
the arg is another format-string and the following arg is a list of arguments;
~? is ~K for backward compatibility with some existing implementations

|#

;; format.scm: written by Doug Currie in 2024

(define-library (owl format)
  (export
    format
  )
  (import
    (owl core)
    (owl math)
    (owl math rational)
    (owl math extra)
    (owl list)
    (owl char)
    (only (owl syscall) error)
    (owl render)
    (owl string)
    (owl io)
    (owl port)
    (owl proof)
    (scheme base)
  )

(begin

  (define (fmt-error irr)
    (error "format: " irr))

  (define (char-digit-val c)
    (- c #\0))

  (define (length-upto lst end)
    (let loop ((lst lst) (n 0))
      (cond
        ((eq? lst end) n)
        ((null? lst) n)
        (else (loop (cdr lst) (+ n 1))))))

  (define (pad-left-with ch ask res tal)
    (let woop ((wid (length-upto res tal)) (res res))
        (if (>= wid ask) res (woop (+ wid 1) (cons ch res)))))

  (define (pad-left ask res tal) (pad-left-with #\space ask res tal))

  (define (render-f num digs tl)
    (cond ((eq? (type num) type-rational)
           (lets ((unum (abs num))
                  (scale (expt 10 digs))
                  (xnum (round (* unum scale)))
                  (q r (truncate/ xnum scale))
                  (tlr (if (> digs 0)
                        (cons #\. (pad-left-with #\0 digs (render-number r tl 10) tl))
                        tl))
                  (tln (render-number q tlr 10)))
              (if (negative? num) (cons #\- tln) tln)))
          ;; complex as two ~f format nums
          ((eq? (type num) type-complex)
           (lets ((nr ni num)
                  (tli (render-f ni digs (cons #\i tl))))
            (render-f nr digs (if (= (car tli) #\-) tli (cons #\+ tli)))))
          ((number? num)
           (lets ((d (expt 10 digs))
                  (tlr (if (> digs 0) (cons #\. (cdr (render-number d tl 10))) tl)))
            (render-number num tlr 10)))
          (else
           (render num tl))))

  (define (render-format tl format-string objects)
    (let loop ((format-list (string->runes format-string))
                (objects objects))
      (cond ((null? format-list) tl)
            ((and (char=? (car format-list) #\~)
                  (not (null? (cdr format-list)))
                  (char=? (cadr format-list) #\newline))
                  ; ~<newline> ignores following whitespace
              (let doop ((format-list (cddr format-list)))
                (if (or (null? format-list)
                        (not (memq (car format-list) '(#\newline #\space #\return #\tab))))
                  (loop format-list objects)
                  (doop (cdr format-list)))))
            ((char=? (car format-list) #\~)
              (let plop ((format-list (cdr format-list))
                          (d-arg 0) ; for ~[w[,d]]? format parameters
                          (w-arg 0) ;  or ~[w[,'c]]? format parameters
                          (in-width? #t))
                (if (null? format-list)
                    (fmt-error "Incomplete escape sequence")
                    (case (car format-list)
                      ((#\% #\&)
                        (cons #\newline (loop (cdr format-list) objects)))
                      ((#\_)
                        (cons #\space (loop (cdr format-list) objects)))
                      ((#\t #\T)
                        (cons #\tab (loop (cdr format-list) objects)))
                      ((#\~)
                        (cons #\~ (loop (cdr format-list) objects)))
                      ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
                        (let ((v (char-digit-val (car format-list))))
                          (plop (cdr format-list)
                              (if in-width? d-arg (+ (* 10 d-arg) v))
                              (if in-width? (+ (* 10 w-arg) v) w-arg)
                              in-width?)))
                      ((#\,)
                        (if in-width?
                            (plop (cdr format-list) d-arg w-arg #f)
                            (fmt-error "too many commas in format directive")))
                      ((#\')
                        (cond
                          (in-width?
                            (fmt-error "unexpected quote in width prefix param"))
                          ((null? (cdr format-list))
                            (fmt-error "Incomplete escape sequence"))
                          ((not (= d-arg 0))
                            (fmt-error "unexpected quote in digits prefix param"))
                          (else
                            (plop (cddr format-list) (cadr format-list) w-arg #f))))
                      ((#\c #\C)
                        (if (null? objects)
                          (fmt-error "No value for escape sequence")
                          (let ((c (car objects)))
                            (if (char? c)
                              (cons c (loop (cdr format-list) (cdr objects)))
                              (fmt-error "Non char arg for ~c")))))
                      ((#\a #\A)
                        (if (null? objects)
                          (fmt-error "No value for escape sequence")
                          (lets ((tal (loop (cdr format-list) (cdr objects)))
                                 (res (render (car objects) tal)))
                            (pad-left w-arg res tal))))
                      ((#\s #\S)
                        (if (null? objects)
                          (fmt-error "No value for escape sequence")
                          (lets ((tal (loop (cdr format-list) (cdr objects)))
                                 (res (render* (car objects) tal)))
                            (pad-left w-arg res tal))))
                      ((#\f #\F)
                        (if (null? objects)
                          (fmt-error "No value for escape sequence")
                          (lets ((tal (loop (cdr format-list) (cdr objects)))
                                 (res (render-f (car objects) d-arg tal)))
                            (pad-left w-arg res tal))))
                      ((#\d #\D #\x #\X #\o #\O #\b #\B)
                        (if (null? objects)
                          (fmt-error "No value for escape sequence")
                          (lets ((tal (loop (cdr format-list) (cdr objects)))
                                 (bas (case (car format-list)
                                            ((#\d #\D) 10) ((#\x #\X) 16)
                                            ((#\o #\O)  8) ((#\b #\B)  2)))
                                 (chr (if (= d-arg 0) #\space d-arg))
                                 (res (render-number (car objects) tal bas)))
                            (pad-left-with chr w-arg res tal))))
                      (else
                        (fmt-error "Unrecognized escape sequence"))))))
            (else
              (cons (car format-list) (loop (cdr format-list) objects))))))

  (define (format to format-string . objects)
    (cond ((eq? to #f)
           (runes->string (render-format '() format-string objects)))
          ((eq? to #t)
           (write-bytes stdout (render-format '() format-string objects)))
          ((port? to)
           (write-bytes to (render-format '() format-string objects)))
          ((or (pair? to) (null? to))
           (render-format to format-string objects))
          ((string? to) ; assume it's a format string
           (runes->string (render-format '() to (cons format-string objects))))
          (else
           (fmt-error "bad destination"))))

  (example
    (format "~a ~s~%" "foo" "bar")            = "foo \"bar\"\n"
    (format "~a ~s ~11a~%" "foo" "bar" "baz") = "foo \"bar\"         baz\n"
    (format "~a ~s ~1a~%" "foo" "bar" "baz")  = "foo \"bar\" baz\n"
    (format "~10,4f~%" 45)                    = "   45.0000\n"
    (format "~10,4f~%" 45.99888)              = "   45.9989\n"
    (format "~10,4f~%" -45.99888)             = "  -45.9989\n"
    (format "~10,4f~%" "foo")                 = "       foo\n"
    (format "~10,2f~%" 1-2/3i)                = "1.00-0.67i\n"

    (format #f "A char: ~c ~%" #\☂)           = "A char: ☂ \n"

    (format #f "#x~8,'0x~%" 253)              = "#x000000fd\n"
    (format #f "#b~8,'0b~%" 253)              = "#b11111101\n"

    (format "~0,4f~%" (/ 1 1000))             = "0.0010\n"
    (format "~0,4f~%" (* 1 1000))             = "1000.0000\n"

    (format #f "~5,'*d" 12) = "***12"
    (format #f "~5,'0d" 12) = "00012"
    (format #f "~3d"  1234) = "1234"  ; width param ignored, too small

    (format #f "⦑ ~c ⦒\n" #\⚝)  = "⦑ ⚝ ⦒\n"
    (format #f "⦑ ~9c ⦒\n" #\⚝) = "⦑ ⚝ ⦒\n" ; width param ignored for ~c

    (format (format '() "bar") "foo") = (string->runes "foobar")

    (format #f "abc~
            ~d def~
            ~d" 1 2)  = "abc1 def2" ; whitespace ignored after ~<newline>
  )

))
