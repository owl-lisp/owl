#| doc
Default Toplevel

Values exported in this library are available when starting owl interactively. The form
(exports ...) allows exporting all values exported by another library.
|#

(define-library (owl toplevel)

   (export
      (exports (owl boolean))
      (exports (owl bytevector))
      (exports (owl core))
      (exports (owl date))
      (exports (owl digest))
      (exports (owl equal))
      (exports (owl fasl))
      (exports (owl ff))
      (exports (owl format))
      (exports (owl function))
      (exports (owl io))
      (exports (owl json))
      (exports (owl lazy))
      (exports (owl list))
      (exports (owl list-extra))
      (exports (owl math extra))
      (exports (owl math))
      (exports (owl os))
      (exports (owl port))
      (exports (owl random))
      (exports (owl regex))
      (exports (owl render))
      (exports (owl rlist))
      (exports (owl sort))
      (exports (owl string))
      (exports (owl symbol))
      (exports (owl syntax-rules))
      (exports (owl syscall))
      (exports (owl time))
      (exports (owl tuple))
      (exports (owl vector))

      halt
      lets/cc
      read
      read-ll
      ref
      suspend
      wait
      (exports (scheme base))
      (exports (scheme cxr))
      (exports (scheme write)))

   (import
      (owl core)
      (owl boolean)
      (owl list)
      (owl alist)
      (owl rlist)
      (owl list-extra)
      (owl tuple)
      (owl ff)
      (owl io)
      (owl port)
      (owl time)
      (owl lazy)
      (owl math extra)
      (owl string)
      (owl symbol)
      (owl sort)
      (owl fasl)
      (owl function)
      (owl bytevector)
      (owl vector)
      (owl equal)
      (owl random)
      (owl regex)
      (owl format)
      (owl render)
      (owl syscall)
      (owl json)
      (owl math)
      (owl digest)
      (owl syntax-rules)
      (only (owl compile) suspend)
      (only (owl sexp) read read-ll)
      (scheme base)
      (scheme cxr)
      (scheme write)

      ;; just pull into the fasl
      (owl date)
      (owl os)
      (owl ext)
      (scheme case-lambda)
      (scheme complex)
      (scheme process-context)
      (scheme time)))
