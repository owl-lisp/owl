#| doc
OS information
|#

(define-library (owl os)
   (export
      unix?
      windows?
      system
      )

   (import
      (owl core)
      (owl io)
      (owl list)
      (owl list-extra)
      (owl equal)
      (owl string)
      (prefix (owl sys) sys-)
      )

   (begin
      (define (unix?)    (has? *features* 'unix))
      (define (windows?) (has? *features* 'win32))

      ;; read ext/win32/dispatch.c
      (define (system args)
         (if (unix?)
            (let ((pid (sys-fork)))
               (cond
                  ((eq? pid #false) ;; fork failed
                     #f)
                  ((eq? pid #true) ;; we're in child process
                     (or (sys-execvp args) (halt 1)))
                  (else
                     ;; otherwise we're in parent process.
                     ;; wait and check if the child process terminates
                     ;; normally with a zero status
                     (equal? (sys-wait pid) '(1 . 0)))))
            ;; hack-ish
            (if (list? args)
               (sys-prim 18 (fold (λ (a b) (string-append a " " b)) "" args) #f #f)
               (sys-prim 18 args #f #f))))
      ))
