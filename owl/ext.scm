#| doc
extension helpers
|#

;; written by Krzysztof Michałczyk <kpm@krzysckh.org> in 2024

(define-library (owl ext)
  (export
   define-main
   define-primops
   define-cflags
   define-ldflags
   cgen
   )

  (import
   (owl core)
   (owl list)
   (owl list-extra)
   (owl format)
   (owl render)
   (owl string)
   (owl io)
   (owl ff)
   (scheme base)
   (scheme cxr)
   )

  (begin
    (define (make-binop sym)
      (cons sym (λ (a b) (format "(~a ~a ~a)" a (str sym) b))))

    (define (make-binops l)
      (fold (λ (a b) (append a (list (make-binop b)))) #n l))

    (define c-env
      (append
       (make-binops '(+ - * / & ^ |\|| = == != < > <= >= ! && |\|\||)) ;; todo: check for more
       (list
        (cons 'set! (λ (a b) (format "(~a = ~a)" a b))))))

    (define (maybe-quote-string s)
      (if (string? s) (str* s) s))

    ;; good luck with floats
    (define (guess-type v)
      (cond
       ((number? v) 'int)
       ((string? v) 'char*)
       (else
        ((error "cannot guess type for " v)))))

    ;; functions are dependant on each-other
    ;; so i pass f-env around with a list of declared function pointers
    ;; this kind of sucks

    ;; ->c x if x is a list
    (define (maybe-unwrap x f-env)
      (let ((exp->c (get f-env 'exp->c)))
        (if (list? x) (exp->c x f-env) x)))

    (define (case->c over lst f-env)
      (let ((exps->c (get f-env 'exps->c))
            (default (assoc 'else lst))
            (opts (filter (λ (x) (not (eq? 'else (car x)))) lst)))
        (format
         #f
         "switch (~a) {~%~a~%}~%"
         (maybe-unwrap over f-env)
         (fold
          string-append
          ""
          (append
           (map
            (λ (v)
              (format
               #f
               "case (~a): {~%~a~% break;};~%"
               (car v) (exps->c (cdr v) f-env)))
            opts)
           (list
            (format #f "default: {~%~a~%};~%" (exps->c (cdr default) f-env))))))))

    (define (exp->c exp f-env)
      (let ((f (car exp))
            (exps->c (get f-env 'exps->c)))
        ;; 1st try to match macros, that don't maybe-unwrap
        (cond
         ((eq? f 'begin)
          (format #f "{~%~a~%}" (exps->c (cdr exp) f-env)))
         ((eq? f 'cast)
          (format #f "((~a)~a)" (cadr exp) (maybe-unwrap (caddr exp) f-env)))
         ((eq? f 'while)
          ;; (format #f "while (~a) ~a" (exp->c (cadr exp) f-env) (exp->c (caddr exp) f-env)))
          (format #f "while (~a) ~a" (maybe-unwrap (cadr exp) f-env) (exp->c (caddr exp) f-env)))
         ((eq? f '&)
          (format #f "(&~a)" (cadr exp)))
         ((eq? f 'case*) ;; case without switch, maybe for extensions? (case* x y)
          (format #f "case (~a): {~%~a~% break;}" (cadr exp) (exps->c (cddr exp) f-env)))
         ((eq? f 'include)
          (format #f "#include <~a>~%" (cadr exp))) ;; newlines at the end because useless semicolons :3
         ((eq? f 'include-local)
          (format #f "#include \"~a\"~%" (cadr exp)))
         ((eq? f 'if)
          ;; (if exp1 exp2 exp3), exp[23] may be a begin block
          (format
           #f
           "if (~a) ~%~a~% else ~%~a~%"
           (exp->c (cadr exp) f-env)
           (exps->c (list (caddr exp)) f-env)
           (exps->c (list (cadddr exp)) f-env)))
         ((eq? f 'when)
          ;; (when exp1 exp ...)
          (format
           #f
           "if (~a) { ~%~a~% }~%"
           (exp->c (cadr exp) f-env)
           (exps->c (cddr exp) f-env)))
         ((eq? f 'case)
          (case->c (cadr exp) (cddr exp) f-env))
         (else
          (let ((args (map (C maybe-unwrap f-env) (map maybe-quote-string (cdr exp))))
                (binop (assoc f c-env)))
            (cond
             (binop
              (let ((f* (cdr binop)))
                (fold (λ (a b) (f* a b)) (f* (car args) (cadr args)) (cddr args))))
             ((eq? f 'let)
              (let ((narg (len args)))
                ;; (let x = 10)
                ;; or (let int x = 10)
                ;; or (let int x)
                (unless (or (= narg 2) (= narg 3) (= narg 4))
                  (error "invalid n of args for let " args))

                (let ((T (case narg
                           ((4 2) (car args))
                           (3 (guess-type (lref args 2)))))
                      (name (case narg
                              ((2 4) (cadr args))
                              (3 (car args)))))
                  (if (> narg 2)
                      (format "~a ~a = ~a" T name (last args 'oops!))
                      (format "~a ~a" T name)))))
             (else
              (format
               #f "~a(~a)" f
               (fold
                (λ (a b)
                  (if (equal? "" a)
                      (str b)
                      (str a ", " b)))
                ""
                args)))))))))

    (define (exps->c lst f-env)
      (fold
       (λ (a b)
         (string-append a b "\n"))
       ""
       (map (λ (e) (format (if (eq? (car* e) 'begin) "~a" "~a;") (exp->c e f-env))) lst)))
    ;; don't add semicolon on the end of begin blocks

    ;; args = ((type name) ...)
    (define (defun type name args body f-env)
      (format
       #f
       "~a~%~a(~a)~%{~%~a~%}~%~%"
       type
       name
       (fold
        (λ (a b)
          (if (equal? "" a)
              (format "~a ~a" (car b) (cadr b))
              (format "~a, ~a ~a" a (car b) (cadr b))))
        ""
        args)
       (exps->c body f-env)))

    (define f-env
      (pipe empty
        (put 'exp->c exp->c)
        (put 'exps->c exps->c)
        (put 'case->c case->c)
        (put 'defun defun)))

    (define-syntax cgen
      (syntax-rules ()
        ((_) "")
        ((_ (tf nf ((ta na) ...) exp ...) rest ...)
         (str
          (defun 'tf 'nf '((ta na) ...) '(exp ...) f-env)
          (_ rest ...)))
        ((_ (exp ...) rest ...)
         (str
          (exps->c '((exp ...)) f-env)
          (_ rest ...)))))

    (define-syntax define-primops
      (syntax-rules (=> case*)
        ((_) "")
        ((_ (n => (exp ...)) rest ...)
          (str
           (cgen (case* n exp ...))
           (_ rest ...)))
        ((_ filename rest ...)
         (let ((f (open-output-file filename)))
           (print-to f (_ rest ...))
           (close-port f)))))

    (define-syntax define-main
      (syntax-rules ()
        ((_ filename exp ...)
         (let ((f (open-output-file filename)))
           (print-to f (cgen exp ...))
           (close-port f)))))

    (define-syntax define-cflags
      (syntax-rules ()
        ((_ cflags)
         (let ((f (open-output-file "CFLAGS")))
           (print-to f (str cflags))
           (close-port f)))))

    (define-syntax define-ldflags
      (syntax-rules ()
        ((_ ldflags)
         (let ((f (open-output-file "LDFLAGS")))
           (print-to f (str ldflags))
           (close-port f)))))
    ))
