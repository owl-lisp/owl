
(import (owl sys))

(print "init -> " (list->file '(42 42 42) "tmp/app"))

(print "start -> " (file->list "tmp/app"))

(print "1st size -> " (cdr (assq 'size (stat "tmp/app" #f))))

(let ((port (open-append-file "tmp/app")))
   (print-to port "aaa")
   (print "2nd size -> " (cdr (assq 'size (stat port #f))))
   (close-port port))

(print "*** + append aaa\\n -> " (file->list "tmp/app"))

(let ((port (open-output-file "tmp/app")))
   (print-to port "bbb")
   (close-port port))

(print "truncate + bbb -> " (file->list "tmp/app"))

(print "remove -> " (unlink "tmp/app"))
