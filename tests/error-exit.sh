#!/bin/sh

: ${CC:="gcc"}

LISP="tmp/foo-$$.scm"
C="tmp/foo-$$.c"
OBJ="./tmp/foo-$$"

echo '(lambda (args) (error "fail" args))' > $LISP
$@ --run "$LISP" 2>/dev/null
echo "lisp exit $?"

$@ -o "$C" "$LISP"
$CC $CFLAGS -o "$OBJ" "$C" $LDFLAGS
$OBJ 2>/dev/null

echo "native exit $?"

rm $LISP $C $OBJ
