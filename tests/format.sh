#!/bin/sh

# check format printing to stdout

LISP="tmp/fmt-src-$$.scm"

echo '(import (owl format))' > $LISP
echo '(format #t "3~%")' >> $LISP

echo '(format #t "~a ~s~%" "foo" "bar")           ' >> $LISP
echo '(format #t "~a ~s ~11a~%" "foo" "bar" "baz")' >> $LISP
echo '(format #t "~a ~s ~1a~%" "foo" "bar" "baz") ' >> $LISP
echo '(format #t "~10,4f~%" 45)                   ' >> $LISP
echo '(format #t "~10,4f~%" 45.99888)             ' >> $LISP
echo '(format #t "~10,4f~%" -45.99888)            ' >> $LISP
echo '(format #t "~10,4f~%" "foo")                ' >> $LISP
echo '(format #t "~10,2f~%" 1-2/3i)               ' >> $LISP

#echo '(format #t "A char: ~c ~%" #\☂)         ' >> $LISP

echo '(format #t "#x~8,'\''0x~%" 253)             ' >> $LISP
echo '(format #t "#b~8,'\''0b~%" 253)             ' >> $LISP

echo '(format #t "~5,'\''*d~%" 12)  ' >> $LISP
echo '(format #t "~5,'\''0d~%" 12)  ' >> $LISP
echo '(format #t "~3d~%"  1234)  ' >> $LISP

#echo '(format #t "⦑ ~c ⦒\n" #\⚝)  ' >> $LISP
#echo '(format #t "⦑ ~9c ⦒\n" #\⚝)  ' >> $LISP

echo '(format #t "abc~
            ~d def~
            ~d" 1 2)    ' >> $LISP

echo '(format #t "~%")' >> $LISP

# cat $LISP

$@ --quiet < $LISP

rm $LISP
