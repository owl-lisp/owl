#include <netinet/in.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <termios.h>
#include <netdb.h>

#define _ADDSOCK(x)
#define closesocket close

static struct termios tsettings;

static void setdown() {
   tcsetattr(0, TCSANOW, &tsettings); /* return stdio settings */
}

/* TODO: implement this in owl */
static word do_poll(word a, word b, word c) {
   fd_set rs, ws, es;
   word *cur;
   hval r1, r2;
   int nfds = 1;
   struct timeval tv;
   int res;
   FD_ZERO(&rs); FD_ZERO(&ws); FD_ZERO(&es);
   for (cur = (word *)a; (word)cur != INULL; cur = (word *)cur[2]) {
      int fd = immval(G(cur[1], 1));
      FD_SET(fd, &rs);
      FD_SET(fd, &es);
      if (fd >= nfds)
         nfds = fd + 1;
   }
   for (cur = (word *)b; (word)cur != INULL; cur = (word *)cur[2]) {
      int fd = immval(G(cur[1], 1));
      FD_SET(fd, &ws);
      FD_SET(fd, &es);
      if (fd >= nfds)
         nfds = fd + 1;
   }
   if (c == IFALSE) {
      res = select(nfds, &rs, &ws, &es, NULL);
   } else {
      hval ms = immval(c);
      tv.tv_sec = ms/1000;
      tv.tv_usec = (ms%1000)*1000;
      res = select(nfds, &rs, &ws, &es, &tv);
   }
   if (res < 1) {
      r1 = IFALSE; r2 = BOOL(res < 0); /* 0 = timeout, otherwise error or signal */
   } else {
      int fd; /* something active, wake the first thing */
      for (fd = 0; ; ++fd) {
         if (FD_ISSET(fd, &rs)) {
            r1 = make_immediate(fd, TPORT); r2 = F(1); break;
         } else if (FD_ISSET(fd, &ws)) {
            r1 = make_immediate(fd, TPORT); r2 = F(2); break;
         } else if (FD_ISSET(fd, &es)) {
            r1 = make_immediate(fd, TPORT); r2 = F(3); break;
         }
      }
   }
   return cons(r1, r2);
}

static void _setup(void) {
   tcgetattr(0, &tsettings);
}
