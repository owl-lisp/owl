(import
 (owl ext))

(define-cflags "")
(define-ldflags "")

(define-main "main.c"
  (int ext_example_owl ((int a) (int b))
    (let int c)
    (set! c (^ a b))
    (printf "ext_example_owl(%d,%d) = %d\n" a b c)
    (return c)))

(define-primops "dispatch.c"
  (48 => ((return (onum (ext_example_owl (cnum a) (cnum b)) 0)))))
