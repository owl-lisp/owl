#| doc
Sqlite wrapper

lisp wrapper for the sqlite extension.
usage:
```scheme
(import
   (prefix (owl sys) sys/)
   (prefix (ext sqlite io) s3/))

(sys/unlink "temp.db")

(let ((ptr (s3/open "temp.db")))
   (s3/execute ptr "create table test (s text)")
   (for-each
      (λ (s) (s3/execute ptr "insert into test (s) values (?)" (list s)))
      (list "a" "b" "c"))
   (print (s3/execute ptr "select rowid, s from test;" #n))
   (s3/close ptr))
```
|#

;; Originally added by Krzysztof Michałczyk <kpm@krzysckh.org>

(define-library (ext sqlite io)
  (import
   (owl toplevel))

  (export
   open
   close
   execute
   )

  (begin
    (define (type! pred v)
      (when (not (pred v))
        (error "invalid type for value " v)))

    (define (open name)
      (type! string? name)
      (sys-prim 100 (c-string name) #f #f))

    (define (close ptr)
      (type! self ptr) ;; ptr is a bytevector, but it might be an opaque type so idc just check if !#f
      (sys-prim 101 ptr #f #f))

    (define execute
      (case-lambda
       ((ptr sql)      ;; execute a statement (without binding and returning values)
        (type! self ptr)
        (type! string? sql)
        (sys-prim 102 ptr (c-string sql) #f))
       ((ptr sql bind) ;; execute an expression (with binding and returning values)
        (type! self ptr)
        (type! string? sql)
        (type! list? bind)
        (reverse (map reverse (sys-prim 103 ptr (c-string sql) (map (λ (v) (if (string? v) (c-string v) v)) bind)))))))
    ))
