(import
 (owl ext))

;; written by Krzysztof Michałczyk <kpm@krzysckh.org> in 2024

(define-cflags "")
(define-ldflags "-lsqlite3")

(define-main "main.c"
  (include "sqlite3.h")
  (word s3err ((char* why))
    (fprintf stderr "sqlite3: non-fatal runtime error: %s" why)
    (return IFALSE)))

(define-primops "dispatch.c"
  (100 => ((let sqlite3* db)
           (if (!= SQLITE_OK (sqlite3_open (cstr a) (& db)))
               (return ITRUE)
               (return (PTR db)))))
  (101 => ((if (== (sqlite3_close (cptr a)) SQLITE_OK)
               (return ITRUE)
               (return (s3err "couldn't close")))))
  (102 => ((let char* err = NULL)
           (if (!= (sqlite3_exec (cptr a) (cstr b) 0 0 (& err)) SQLITE_OK)
               (return (s3err "couldn't exec"))
               (return ITRUE))))
  (103 => ((let sqlite3_stmt* res)
           (let i = 1)
           (let int v = (sqlite3_prepare_v2 (cptr a) (cstr b) -1 (& res) 0))
           (let word ret = INULL)
           (let word l)
           (when (!= v SQLITE_OK)
             (return (s3err "couldn't prepare")))
           (while (!= c INULL)
             (begin
               (if (is_type (car c) TNUM)
                   (begin
                     (sqlite3_bind_int res i (cnum (car c)))
                     (set! i (+ i 1)))
                   (if (stringp (car c))
                       (begin
                         (sqlite3_bind_text res i (cstr (car c)) -1 SQLITE_STATIC)
                         (set! i (+ i 1)))
                       (return (s3err "unsupported type for bind"))))
               (set! c (cdr c))))
           (while (== (sqlite3_step res) SQLITE_ROW)
             (begin
               (set! l INULL)
               (set! i 0)
               (while (< i (sqlite3_column_count res))
                      (begin
                        (case (sqlite3_column_type res i)
                          (SQLITE_INTEGER (set! l (cons (onum (sqlite3_column_int res i) 1) l)))
                          (SQLITE_TEXT    (set! l (cons (mkstring (cast char* (sqlite3_column_text res i))) l)))
                          (SQLITE_NULL    (set! l (cons IFALSE l)))
                          (else
                           (abort)))
                        (set! i (+ i 1))))
               (set! ret (cons l ret))))

           (when (!= SQLITE_OK (sqlite3_finalize res))
             (return (s3err "couldn't finalize")))

           (return ret))))
