/* This file is included in the VM */

int ext_example(int a, int b) {
  int c = a ^ b;
  printf("ext_example(%d,%d) = %d\n", a, b, c);
  return c;
}
