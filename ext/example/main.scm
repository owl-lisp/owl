
(define-library (ext example main)

   (import
      (owl toplevel)
      (owl proof))

   (export
      test-call)

   (begin

      (define (test-call a b)
         (sys-prim 49 a b #f))

      (example
         (test-call 11 22) = (bxor 11 22)
         (test-call 111 222) = (bxor 111 222))

      (print "Example library OK")))
