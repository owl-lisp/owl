## Primop availability per OS

|id  |name          |os-agnostic|unix|windows|
|----|--------------|-----------|----|-------|
|0   |clock-gettime |no         |yes |yes    |
|1   |open          |no         |yes |yes    |
|2   |close         |no         |yes |yes    |
|3   |sopen         |no         |yes |yes    |
|4   |accept        |yes        |-   |-      |
|5   |read          |no         |yes |yes    |
|6   |exit          |yes        |-   |-      |
|7   |setmemlimit   |yes        |-   |-      |
|8   |sysconst      |yes        |-   |-      |
|9   |procvar       |no         |yes |yes    |
|10  |recv-udp      |yes        |-   |-      |
|11  |open-dir      |yes        |-   |-      |
|12  |read-dir      |yes        |-   |-      |
|13  |close-dir     |yes        |-   |-      |
|14  |strerror      |yes        |-   |-      |
|15  |fcntl         |yes        |-   |-      |
|16  |getenv        |yes        |-   |-      |
|17  |execv         |yes        |-   |-      |
|18  |fork          |no         |yes |no*    |
|19  |wait          |no         |yes |no     |
|20  |chdir         |yes        |-   |-      |
|21  |kill          |no         |yes |no     |
|22  |unlink        |yes        |-   |-      |
|23  |rmdir         |yes        |-   |-      |
|24  |mknod         |no         |yes |~      |
|25  |lseek         |yes        |-   |-      |
|26  |termctl       |no         |yes |no     |
|27  |sendmsg       |yes        |-   |-      |
|28  |setenv        |no         |yes |yes    |
|29  |connect       |yes        |-   |-      |
|30  |dup2          |yes        |-   |-      |
|31  |pipe          |no         |yes |yes    |
|32  |rename        |no         |yes |yes    |
|33  |link          |no         |yes |no     |
|34  |symlink       |no         |yes |no     |
|35  |readlink      |no         |yes |no     |
|36  |getcwd        |yes        |-   |-      |
|37  |umask         |yes        |-   |-      |
|38  |stat          |no         |yes |yes    |
|39  |chmod         |no         |yes |no     |
|40  |chown         |no         |yes |no     |
|41  |peek          |yes        |-   |-      |
|42  |write         |no         |yes |yes    |
|43  |poll          |no         |yes |yes    |
|44  |getaddrinfo   |yes        |-   |-      |
|45  |getpid        |yes        |-   |-      |
|46  |catch-signals |no         |yes |yes    |
|47  |isatty        |yes        |-   |-      |

\* fork is changed to ShellExecute on win32 to allow process simple process spawning as win32 doesn't have a fork-like api. It can be accessed via the `(system)` function from `(owl os)`.
